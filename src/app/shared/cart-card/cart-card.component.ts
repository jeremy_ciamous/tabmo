import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Product } from 'src/app/products/product';
import { State } from 'src/app/state/app.state';
import {
  addProduct,
  removeOneProduct,
} from '../../cart/state/actions/cart-page.actions';

@Component({
  selector: 'app-cart-card',
  templateUrl: './cart-card.component.html',
  styleUrls: ['./cart-card.component.scss'],
})
export class CartCardComponent implements OnInit {
  @Input() product: Product;
  @Input() isEditable: boolean
  constructor(private store: Store<State>) {}

  ngOnInit(): void {}
  addOne() {
    this.store.dispatch(addProduct({ selectedProduct: this.product }));
  }
  removeOne() {
    this.store.dispatch(removeOneProduct({ selectedProduct: this.product }));
  }
  getPrice(product: Product): number {
    return product.nombreSelectionne * product.price;
  }
}
