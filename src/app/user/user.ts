export interface UserState {
  name:string;
  lastname:string;
  email:string;
  accessToken:string;
  refreshToken:string;
  error:string;
}
export interface User {
  name:string;
  lastname:string;

}
export interface Credential  {
  email:string
  password:string
  }
export interface ApiError {
  error: string
}
