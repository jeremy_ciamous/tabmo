import { HttpClient } from '@angular/common/http'
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { User } from '../user'
import { UserService } from '../user.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
users:User[]
  loginForm: FormGroup;
  // constructor(private router: Router) {}
  constructor(private router: Router, private userService: UserService){
    this.userService.authent({email:'johndoe@gmail.com',password:'admin'}).subscribe(console.log)

  }
  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }

  onSubmit() {
    // Vérifiez si les informations d'identification sont valides ici

      // Sinon, affichez un message d'erreur ou effectuez une autre action appropriée
      console.log('Invalid credentials');

  }
}
