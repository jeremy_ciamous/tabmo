import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserService } from '../user.service';
import * as UserAction from '../state/actions/user-api.actions';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import { UserState } from '../user';
import { of } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class UserEffects {
  constructor(private actions$: Actions, private userService: UserService) {}
  authentUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserAction.authent),
      exhaustMap((action) =>
        this.userService
          .authent(action.user)
          .pipe(map((user: UserState) => UserAction.authentSuccess({ user })))
      ),
      catchError((error)=> of(UserAction.authentFail({error})))
    );
  });
}
