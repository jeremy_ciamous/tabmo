import { createReducer, on } from '@ngrx/store';
import { UserState } from '../user'
import * as UserAction from '../state/actions/user-api.actions';

const initialState: UserState = {
  name:'',
  lastname:'',
  email:'',
  accessToken:'',
  refreshToken:'',
  error:''
};
export const userReducer = createReducer<UserState>(
  initialState,
on(UserAction.authentSuccess,(state,action):UserState=>{
  return{
    ...state,
    name:action.user.name,
    lastname:action.user.lastname,
    email:action.user.email,
    accessToken:action.user.accessToken,
    refreshToken:action.user.refreshToken,
    error:'',
  }
}),
on(
  UserAction.authentFail,
  (state, action): UserState => {
    return {
      ...state,
      error: action.error,
    };
  }
),
on(UserAction.logout, (state) => initialState),
);
