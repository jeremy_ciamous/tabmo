import { createAction, props } from '@ngrx/store';
import { Credential, UserState } from '../../user'

export const authent = createAction(
  '[User API] Login',
  props<{ user:Credential}>()
);

export const authentSuccess = createAction(
  '[User API] Authent Success',
  props<{ user: UserState }>()
);

export const authentFail = createAction(
  '[User API] Authent Fail',
  props<{ error: string }>()
);
export const logout = createAction(
  '[User API] logout'
);

