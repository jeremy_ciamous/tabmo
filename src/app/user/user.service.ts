import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { ApiError, Credential, UserState } from './user'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:8000/login';

  constructor(private http: HttpClient) {
  }

  authent(body: Credential): Observable<UserState| ApiError> {
    return this.http.post<any>(this.baseUrl,body);
  }
}
