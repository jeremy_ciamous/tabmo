import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import * as data from '../../assets/mock-data/users.json';
import { Credential } from '../user/user';

const urls = [
  {
    url: 'http://localhost:8000/login',
    json: data,
  },
];

@Injectable()
export class HttpMockRequestInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    for (const element of urls) {
      if (request.url === urls[0].url) {
        const userCredentials: Credential = request.body;
        if (
          userCredentials.email === 'johndoe@gmail.com' &&
          userCredentials.password === 'admin'
        ) {
          return of(
            new HttpResponse({
              status: 200,
              body: (element.json as any).default,
            })
          );
        } else {
          return of(
            new HttpResponse({
              status: 400,
              body: { error: 'invalid credential' },
            })
          );
        }
      } else if (request.url === element.url) {
        debugger;
        console.log('Loaded from json : ' + request.url);
        return of(
          new HttpResponse({ status: 200, body: (element.json as any).default })
        );
      }
    }
    console.log('Loaded from http call :' + request.url);
    return next.handle(request);
  }
}
