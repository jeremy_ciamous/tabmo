import { HttpRequest, HttpResponse } from '@angular/common/http'
import { of } from 'rxjs'
import * as data from '../../assets/mock-data/countries.json';

let countries: any[] = (data as any).default;
const getCountries = (request: HttpRequest<any>) => {
  return of(new HttpResponse({
    status: 200, body: countries
  }));
};
const getCountry = (request: HttpRequest<any>) => {
  const id = extractIdPathParamFromUrl(request);
  const country = countries.find(c => c.id === id);
  return of(new HttpResponse({
    status: 200, body: country
  }));
};
const extractIdPathParamFromUrl = (request: HttpRequest<any>) => {
  const requestUrl = new URL(request.url);
  return requestUrl.pathname.split('/').pop();
};
export const selectHandler = (request: HttpRequest<any>) => {
  const requestUrl = new URL(request.url);
  const getOneRegexp: RegExp = new RegExp(`/countries/[0-9a-zA-Z]+`);
  switch (request.method) {
    case 'GET':
      const pathname = requestUrl.pathname;
      if (pathname === '/countries') {
        return getCountries;
      } else if (getOneRegexp.test(pathname)) {
        return getCountry;
      } else {
        return null;
      }
    default:
      return null;
  }
};
