import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseCompletedComponent } from './purchase-completed.component';
import { RouterModule, Routes } from '@angular/router';

const purchaseCompletedRoute: Routes = [
  { path: '', component: PurchaseCompletedComponent },
];

@NgModule({
  declarations: [PurchaseCompletedComponent],
  imports: [CommonModule, RouterModule.forChild(purchaseCompletedRoute)],
})
export class PurchaseCompletedModule {}
