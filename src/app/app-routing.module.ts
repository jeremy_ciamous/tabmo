import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './home/not-found/not-found.component';
import { PageComponent } from './home/page/page.component';
import { WelcomeComponent } from './home/welcome/welcome.component';

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      { path: 'welcome', component: WelcomeComponent },
      {
        path: 'login',
        loadChildren: () =>
          import('./user/user.module').then((m) => m.UserModule),
      },
      {
        path: 'products',
        loadChildren: () =>
          import('./products/products.module').then((m) => m.ProductsModule),
      },
      {
        path: 'are-you-sure',
        loadChildren: () =>
          import('./pre-validation/pre-validation.module').then(
            (m) => m.PreValidationModule
          ),
      },
      {
        path: 'congratulation',
        loadChildren: () =>
          import('./purchase-completed/purchase-completed.module').then(
            (m) => m.PurchaseCompletedModule
          ),
      },
      { path: '', redirectTo: 'welcome', pathMatch: 'full' },
    ],
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
