import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Product } from '../../products/product';
import {
  State,
  getCartProducts,
  getTotalPrice,
  getToggleButtonCart,
} from '../state';
import { CartPageActions } from '../state/actions';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  products$: Observable<Product[]>;
  totalPrice$: Observable<number>;
  toggleButtonCart$: Observable<boolean>;
  constructor(private store: Store<State>) {}

  ngOnInit(): void {
    this.products$ = this.store.select(getCartProducts);
    this.totalPrice$ = this.store.select(getTotalPrice);
    this.toggleButtonCart$ = this.store.select(getToggleButtonCart);
  }
  getNumberProducts(products: Product[]): number {
    let totalArcticle = 0;
    products.forEach((product) => {
      totalArcticle += product.nombreSelectionne;
    });
    return totalArcticle;
  }
  toggleButtonCart() {
    this.store.dispatch(CartPageActions.toggleButtonCart());
  }
}
