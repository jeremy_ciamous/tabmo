import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Product } from 'src/app/products/product';
import * as AppState from '../../state/app.state';

export interface State extends AppState.State {
  cart: CartState;
}
export interface CartState {
  products: Product[];
  totalPrice: number;
  toggleButtonCart: boolean;
}
const getCartFeatureState = createFeatureSelector<CartState>('cart');

export const getCart = createSelector(getCartFeatureState, (state) => state);
export const getCartProducts = createSelector(
  getCartFeatureState,
  (state) => state.products
);
export const getTotalPrice = createSelector(
  getCartFeatureState,
  (state) => state.totalPrice
);
export const getToggleButtonCart = createSelector(
  getCartFeatureState,
  (state) => state.toggleButtonCart
);
