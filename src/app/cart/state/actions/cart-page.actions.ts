import { createAction, props } from '@ngrx/store';
import { Product } from 'src/app/products/product';

export const toggleButtonCart = createAction('[Cart] toggle button cart');

export const addProduct = createAction(
  '[Cart] Add a product',
  props<{ selectedProduct: Product }>()
);

export const removeOneProduct = createAction(
  '[Cart] remove one product',
  props<{ selectedProduct: Product }>()
);

export const clearCart = createAction('[Cart] clear Cart');
