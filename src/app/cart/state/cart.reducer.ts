import { createReducer, on } from '@ngrx/store';
import { Product } from 'src/app/products/product';
import { CartState } from '.';
import { CartPageActions } from './actions';

const initialState: CartState = {
  products: [],
  totalPrice: 0,
  toggleButtonCart: false,
};
export const cartReducer = createReducer<CartState>(
  initialState,

  on(
    CartPageActions.addProduct,
    (state, action): CartState => {
      let productAlreadyInTheList = false;
      let newTotalPrice = 0;
      let updateProducts: Product[] = state.products.map((item: Product) => {
        let nombreSelectionne = item.nombreSelectionne;

        if (action.selectedProduct.id === item.id) {
          nombreSelectionne += 1;
          productAlreadyInTheList = true;
        }
        newTotalPrice += item.price * nombreSelectionne;
        return {
          ...item,
          nombreSelectionne: nombreSelectionne,
        };
      });
      if (!productAlreadyInTheList) {
        const selectedProduct: Product = {
          ...action.selectedProduct,
          nombreSelectionne: 1,
        };
        updateProducts = state.products.concat(selectedProduct);
        newTotalPrice += selectedProduct.price;
      }
      return {
        ...state,
        products: updateProducts,
        totalPrice: newTotalPrice,
      };
    }
  ),
  on(
    CartPageActions.removeOneProduct,
    (state, action): CartState => {
      const newTotalPrice = state.totalPrice - action.selectedProduct.price;
      const mappeProduct = state.products.map((item: Product) => {
        let nombreSelectionne = item.nombreSelectionne;
        if (item.id === action.selectedProduct.id) {
          nombreSelectionne -= 1;
        }
        return {
          ...item,
          nombreSelectionne: nombreSelectionne,
        };
      });
      const updateProducts = mappeProduct.filter((item) => {
        if (item.nombreSelectionne > 0) {
          return item;
        }
      });

      return {
        ...state,
        products: updateProducts,
        totalPrice: newTotalPrice,
      };
    }
  ),
  on(
    CartPageActions.toggleButtonCart,
    (state): CartState => {
      return {
        ...state,
        toggleButtonCart: !state.toggleButtonCart,
      };
    }
  ),
  on(
    CartPageActions.clearCart,
    (): CartState => {
      return initialState;
    }
  )
);
