import { ComponentFixture, TestBed } from '@angular/core/testing';
import {  StoreModule } from '@ngrx/store';
import { cartReducer } from '../cart/state/cart.reducer';
import { productReducer } from '../products/state/product.reducer';

import { PreValidationComponent } from './pre-validation.component';

describe('PreValidationComponent', () => {
  let component: PreValidationComponent;
  let fixture: ComponentFixture<PreValidationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PreValidationComponent],
      imports: [
        StoreModule.forRoot({
          cart: cartReducer,
          products: productReducer,
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
