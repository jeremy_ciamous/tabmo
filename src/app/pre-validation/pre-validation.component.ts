import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getCartProducts, getTotalPrice } from '../cart/state';
import { CartPageActions } from '../cart/state/actions';
import { Product } from '../products/product';
import { State } from '../state/app.state';

@Component({
  selector: 'app-pre-validation',
  templateUrl: './pre-validation.component.html',
  styleUrls: ['./pre-validation.component.scss'],
})
export class PreValidationComponent implements OnInit {
  products$: Observable<Product[]>;
  totalPrice$: Observable<number>;
  constructor(private store: Store<State>) {}

  ngOnInit(): void {
    this.products$ = this.store.select(getCartProducts);
    this.totalPrice$ = this.store.select(getTotalPrice);
  }
  resetCart() {
    this.store.dispatch(CartPageActions.clearCart());
  }
}
