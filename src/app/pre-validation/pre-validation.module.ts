import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreValidationComponent } from './pre-validation.component';
import { StoreModule } from '@ngrx/store';
import { cartReducer } from '../cart/state/cart.reducer';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

const prevalidationRoutes: Routes = [
  { path: '', component: PreValidationComponent },
];

@NgModule({
  declarations: [PreValidationComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(prevalidationRoutes),
    StoreModule.forFeature('cart', cartReducer),
  ],
})
export class PreValidationModule {}
