import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';

import { Product } from '../product';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductsListComponent implements OnInit {
  @Input() products: Product[];
  @Input() errorMessage: string;
  @Input() actualPage: number;
  @Input() totalCount: number;
  @Input() pageSize: number;
  @Input() cartProducts: Product[];
  @Output() pageChanged = new EventEmitter<number>();
  @ViewChild('top') topElement: ElementRef;
  constructor() {}

  ngOnInit(): void {}
  getCartProduct(id: string): Product {
    let product = this.cartProducts.find((product) => product.id === id);
    if (!product) {
      product = this.products.find((product) => product.id === id);
    }
    return product;
  }
  getPage(selectedPage: number) {
    this.pageChanged.emit(selectedPage);
    this.scrollToTop();
  }
  scrollToTop() {
    const y = this.topElement.nativeElement.getBoundingClientRect().top - 60;
    window.scrollTo({ top: y, behavior: 'smooth' });
  }
}
