export interface ProductState {
  data: Product[];
  page: number;
  pageSize: number;
  count: number;
  totalCount: number;
  error: string;
}
export class Product {
  constructor(
    public id: string,
    public name: string,
    public images: {
      small: string;
      large: string;
    },
    public price: number,
    public nombreSelectionne = 0
  ) {}
}
