import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Product } from '../product';

import {
  getError,
  getProducts,
  getCurrentPage,
  getTotalPage,
  getPageSize,
  State,
} from '../state';
import { getCartProducts } from '../../cart/state';
import { ProductPageActions } from '../state/actions';
@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
})
export class PageComponent implements OnInit {
  products$: Observable<Product[]>;
  errorMessage$: Observable<string>;
  currentPage$: Observable<number>;
  totalCount$: Observable<number>;
  pageSize$: Observable<number>;
  cartProducts$: Observable<Product[]>;

  constructor(private store: Store<State>) {}

  ngOnInit(): void {
    this.store.dispatch(ProductPageActions.loadProducts({ page: 1 }));
    this.products$ = this.store.select(getProducts);
    this.errorMessage$ = this.store.select(getError);
    this.currentPage$ = this.store.select(getCurrentPage);
    this.totalCount$ = this.store.select(getTotalPage);
    this.pageSize$ = this.store.select(getPageSize);
    this.cartProducts$ = this.store.select(getCartProducts);
  }
  pageChanged(selectedPage: number) {
    this.store.dispatch(
      ProductPageActions.loadProducts({ page: selectedPage })
    );
  }
}
