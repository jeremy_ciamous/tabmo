import { createReducer, on } from '@ngrx/store';
import { ProductState } from '../product';
import { ProductApiActions, ProductPageActions } from './actions';

const initialState: ProductState = {
  data: [],
  page: 1,
  pageSize: 1,
  count: 0,
  totalCount: 0,
  error: '',
};
export const productReducer = createReducer<ProductState>(
  initialState,

  on(
    ProductApiActions.loadProductsSuccess,
    (state, action): ProductState => {
      return {
        ...state,
        data: action.products.data,
        page: action.products.page,
        pageSize: action.products.pageSize,
        count: action.products.count,
        totalCount: action.products.totalCount,
        error: '',
      };
    }
  ),
  on(
    ProductApiActions.loadProductsFail,
    (state, action): ProductState => {
      return {
        ...state,
        data: [],
        page: 1,
        pageSize: 1,
        count: 0,
        totalCount: 0,
        error: action.error,
      };
    }
  )
);
