import { createAction, props } from '@ngrx/store';
import { ProductState } from '../../product';

export const loadProductsSuccess = createAction(
  '[Product API] Load Products Success',
  props<{ products: ProductState }>()
);

export const loadProductsFail = createAction(
  '[Product API] Load Products Fail',
  props<{ error: string }>()
);
