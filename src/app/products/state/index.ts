import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as AppState from '../../state/app.state';
import { ProductState } from '../product';

export interface State extends AppState.State {
  products: ProductState;
}

const getProductFeatureState = createFeatureSelector<ProductState>('products');

export const getTotalPage = createSelector(
  getProductFeatureState,
  (state) => state.totalCount
);

export const getCurrentPage = createSelector(
  getProductFeatureState,
  (state) => state.page
);
export const getPageSize = createSelector(
  getProductFeatureState,
  (state) => state.pageSize
);
export const getError = createSelector(
  getProductFeatureState,
  (state) => state.error
);
export const getProducts = createSelector(
  getProductFeatureState,
  (state) => state.data
);
