import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Product, ProductState } from './product';
@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private pageSize = '50';
  private productsUrl =
    'https://api.pokemontcg.io/v2/cards?pageSize=' + this.pageSize;

  constructor(private http: HttpClient) {}

  getProducts(page: number): Observable<ProductState> {
    const queryUrl = '&&page=' + page.toString();
    return this.http.get<any>(this.productsUrl + queryUrl).pipe(
      map(
        (data: any): ProductState => {
          return {
            ...data,
            data: data.data
              .map(
                (item: any): Product =>
                  new Product(
                    item?.id,
                    item?.name,
                    item?.images,
                    item?.tcgplayer?.prices?.holofoil?.mid
                      ? item?.tcgplayer?.prices?.holofoil?.mid
                      : 0
                  )
              )
              .filter((itemNoPrice: Product) => itemNoPrice.price !== 0),
          };
        }
      ),
      catchError(this.handleError)
    );
  }

  private handleError(err: any) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }
}
