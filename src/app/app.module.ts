import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './home/welcome/welcome.component';
import { NotFoundComponent } from './home/not-found/not-found.component';
import { MenuComponent } from './home/menu/menu.component';
import { PageComponent } from './home/page/page.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule, registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { CartComponent } from './cart/cart/cart.component';

import { cartReducer } from './cart/state/cart.reducer';
import { SharedModule } from './shared/shared.module';
import { HttpMockRequestInterceptor } from './mock/http-mock-api.interceptor'



export const isMock = environment.mock;
registerLocaleData(localeFr);
@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    PageComponent,
    NotFoundComponent,
    MenuComponent,
    CartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    SharedModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature('cart', cartReducer),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([]),
  ],

  providers: [
    ...isMock ? [{
      provide: HTTP_INTERCEPTORS,
      useClass: HttpMockRequestInterceptor,
      multi: true
      }] : []
    ,{
      provide: LOCALE_ID,
      useValue: 'fr-FR',
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
